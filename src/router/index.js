import Vue from "vue";
import VueRouter from "vue-router";
import Projects_page from "../pages/projects_page.vue";
import HelloWorld from "../components/HelloWorld.vue";

import Home from "../components/Home.vue";


import Employees from "../components/Employees/Employees.vue"

Vue.use(VueRouter);

export const router = new VueRouter({
  routes: [
    {
      path: "*",
      component: HelloWorld
    },
    {
        path: "/projects",
        component: Projects_page
    },
    {
        path: '/employees',
        component: Employees
    },
    {
        path: '/home',
        component: Home
    }
  ]
});

// router.beforeEach((to, from, next) => {
//   const publicPages = ['/public', ];
//   const authRequired = !publicPages.includes(to.path);
//   const loggedIn = localStorage.getItem('user-token');

//   if (authRequired && !loggedIn) {
//     // return next('/public/reg/step1');
//   }

//   next();
// })
