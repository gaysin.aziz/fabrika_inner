import { authHeader } from '../_helpers'
// import axios from 'axios'

export const loanService = { getAll, getByKey }

function getAll() {
    console.log('service get_all loans')
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${process.env.VUE_APP_API}/tickets/`, requestOptions).then(handleResponse);
}

function getByKey(key) {
    console.log('service getByKey loans', key)
    const requestOptions = {
        method: 'get',
        headers: { 'Content-Type': 'application/json; charset=utf-8', ...authHeader() },
    };
    console.log('service getByKey loans', key, requestOptions)


    return fetch(`${process.env.VUE_APP_API}/tickets/${key}/`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
    console.log('handleResponse ', response)
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }
        return data;
    });
}