export function authHeader() {
    let token = localStorage.getItem('user-token');

    if (token) {
        return { 'Authorization': 'Token ' + token };
    } else {
        return {};
    }
}