import Vuex from 'vuex'
import Vue from 'vue'

import {employees} from './modules/employees.module'
import {customers} from './modules/customers.module'
import {services} from './modules/services.module'

Vue.use(Vuex)

export const store = new Vuex.Store({
  modules: {
    employees, customers, services
  },

  state: {
    
  },

  getters: {
  
  },

  mutations: {
  
  },

  actions: {
  }
});