import { employeeService } from '../../_services';

export const employees = {
    namespaced: true,
    state: {
        all: {
            items: [
                {
                    id: 1,
                    first_name: 'Азиз',
                    last_name: 'Гайсин',
                    middle_name: 'Батырович',
                    username: 'Narrador',
                    email: 'gaysin.aziz@gmail.com',
                    job_email: 'a.gaysin@thefactory.kz',
                    mobile: '+77472105135',
                    created_at: new Date(),
                    projects: [
                        {
                            name: 'Песси 2020',
                            code: 'pepsi2020',
                            role: 'frontend'
                        },
                        {
                            name: 'Астра ломбард',
                            code: 'pepsi2020',
                            role: 'frontend'
                        }
                    ]
                },

                {
                    id: 2,
                    first_name: 'Мирас',
                    last_name: 'Кайполдаев',
                    middle_name: 'Еркинович',
                    username: 'jaskinzer',
                    email: 'jaskinzer.jaskinzer@gmail.com',
                    job_email: 'job.jaskinzer@thefactory.kz',
                    mobile: '+77472105135',
                    created_at: new Date(),
                    projects: [
                        {
                            name: 'МТС Жилищные комплексы',
                            code: 'mts',
                            role: 'frontend'
                        },
                        {
                            name: 'Астра ломбард',
                            code: 'pepsi2020',
                            role: 'frontend'
                        }
                    ]
                }
            ]
        }
    },
    actions: {
        getAll({ commit }) {
            employeeService.getAll().then(
                data => commit('getAllSuccess', data),
                error => commit('getAllFailure', error)
            )
        },
    },
    mutations: {
        getAllSuccess(state, loans) {
            state.all = { items: loans };
        },
        getAllFailure(state, error) {
            state.all = { error }
        },

        getByKeySuccess(state, loan) {
            state.loan = loan;
        },
        getByKeyFailure(state, error) {
            state.all = { error }
        },


    },
    getters: {
        LOANS: state => {
            return state.all.items
        },
        ACTIVE_LOANS: state => {
            if (!state.all.items)
                return []
            return state.all.items.filter(x => !x.closing_date)
        },
        PASSIVE_LOANS: state => {
            return state.all.items.filter(x => x.closing_date)
        },
        TRANSACTION_LIST_BY_LOANID: state => id => {
            return state.all.items.find(x => x.id === id).transactions
        },
        LOAN_BY_ID: state => id => {
            return state.all.items.find(x => x.uuid === id)
        },
        LOAN_BY_KEY(state) {
            return key => {
                return state.all.items.find(x => x.key === key)
            }
        },
        LOAN: state => state.loan
    }
}