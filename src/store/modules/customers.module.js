  export const customers = {
    namespaced: true,
    state: {
        all: {
            items: [
                {
                  name: "Pepsi",
                  contacts: [
                    {
                      name: "Клиент Клиентович",
                      role: "Проектный менеджер",
                      type: "customer",
                      phone: ["+77472105135", "++77770293300"],
                      email: ["gaysin.aziz@gmail.com", "a.gaysin@thefactory.kz"]
                    }
                  ]
                },
                {
                  name: "Астра ломбард",
                  contacts: [
                    {
                      name: "Клиент Клиентович",
                      role: "Проектный менеджер",
                      type: "customer",
                      phone: ["+77472105135", "++77770293300"],
                      email: ["gaysin.aziz@gmail.com", "a.gaysin@thefactory.kz"]
                    }
                  ]
                }
              ]
        }
    },
    actions: {
        // getAll({ commit }) {
        //     employeeService.getAll().then(
        //         data => commit('getAllSuccess', data),
        //         error => commit('getAllFailure', error)
        //     )
        // },
    },
    mutations: {
        getAllSuccess(state, loans) {
            state.all = { items: loans };
        },
        getAllFailure(state, error) {
            state.all = { error }
        },

        getByKeySuccess(state, loan) {
            state.loan = loan;
        },
        getByKeyFailure(state, error) {
            state.all = { error }
        },


    },

}